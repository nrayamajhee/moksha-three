use three::{
    self, Object,
    Button, Mesh,
    Key as KeyCode,
    controls::axis::Key,
};
use cgmath::{
    InnerSpace, EuclideanSpace , Decomposed, Transform,
    Point3, Vector4, Vector3,
    Quaternion, Rotation, Rad, Rotation3,
};
use mint;
use bodies::KinematicBody;
// Axis for left and right arrow keys.
pub const AXIS_A_D: Key = Key {
    neg: KeyCode::A,
    pos: KeyCode::D,
};
// Axis for up and down arrow keys.
pub const AXIS_W_S: Key = Key {
    neg: KeyCode::S,
    pos: KeyCode::W,
};
// Axis for up and down arrow keys.
pub const AXIS_8_5: Key = Key {
    neg: KeyCode::Numpad5,
    pos: KeyCode::Numpad8,
};
// Axis for up and down arrow keys.
pub const AXIS_4_6: Key = Key {
    neg: KeyCode::Numpad4,
    pos: KeyCode::Numpad6,
};
pub struct DynamicController {
    object: three::object::Base,
    pub body: KinematicBody,
    pub transform: Decomposed<Vector3<f32>, Quaternion<f32>>,
    initial_transform: Decomposed<Vector3<f32>, Quaternion<f32>>,
    target: Point3<f32>,
    pub position: Point3<f32>,
    pub accel: f32,
    pub velocity: Vector3<f32>,
    pub target_orb: Mesh,
    speed: f32,
    button: Option<Button>,
}
impl DynamicController {
    pub fn new<T: Object>(factory: &mut three::Factory, object: &T) -> Self {
        let body = KinematicBody::new(factory,"data/Plane/plane.gltf");
        let target: mint::Point3<f32> = [0.0,0.0,0.0].into();
        let position: mint::Point3<f32> = [0.0,2.0,-5.0].into();

        let dir = (Point3::from(position) - Point3::from(target)).normalize();
        let up = Vector3::unit_y();
        let q = Quaternion::look_at(dir, up.into()).invert();

        object.set_transform(position, q, 1.0);

        let transform = Decomposed {
                disp: mint::Vector3::from(position).into(),
                rot: q,
                scale: 0.0,
        };
        let cam_orb = {
            let geometry = three::Geometry::uv_sphere(0.5, 64, 128);
            let material = three::material::Phong {
                color: 0xFF0000,
                glossiness: 20.0,
            };
            factory.mesh(geometry, material)
        };
        cam_orb.set_position([0.0,0.0,0.0]);
        DynamicController {
            object: object.upcast(),
            body,
            target: target.into(),
            position: position.into(),
            accel: 0.0,
            velocity: [0.0,0.0,0.0].into(),
            target_orb: cam_orb,
            transform: transform.clone(),
            initial_transform: transform,
            speed: 1.0,
            button: None,
        }
    }
    pub fn update(&mut self, input: &three::Input, dt: f32) {
        // get keys movement 
        let delta_disp = get_delta_disp(input, dt);
        // get mouse movement 
        let mut mouse_delta = [0.0, 0.0].into();
        match self.button {
            Some(button) => if input.hit(button) { mouse_delta = input.mouse_delta_raw()
                },
            None => mouse_delta = input.mouse_delta_raw()
        }
        // if mouse movement or key movement
        // Pull the camera to the center
        let pre = Decomposed {
            disp: -self.target.to_vec(),
            ..Decomposed::one()
        };
        // Scale the camera, and apply the rotations
        let axis = self.transform.rot * Vector3::unit_x();
        let q_ver = Quaternion::from_axis_angle(axis, Rad(self.speed * (0.001 * -mouse_delta.y)) + Rad(0.5 * dt * -delta_disp.x));
        let axis = self.body.transform.rot * Vector3::unit_y();
        let q_hor = Quaternion::from_axis_angle(axis, Rad(self.speed * (0.001 * -mouse_delta.x)) + Rad(0.5 * dt * -delta_disp.y));
        let axis = self.body.transform.rot * Vector3::unit_z();
        let roll = Quaternion::from_axis_angle(axis, Rad(0.5 * dt * delta_disp.z));
        let dw = delta_disp.w;
        self.accel += dw as f32 / 10.0;
        let disp = [axis.x * self.accel * dt, axis.y * self.accel * dt, axis.z * self.accel * dt];
        let next_pos = [self.body.transform.disp.x + disp[0], self.body.transform.disp.y + disp[1], self.body.transform.disp.z + disp[2]];
        let post = Decomposed {
            disp: self.target.to_vec(),
            scale: 1.0 + input.mouse_wheel() / 100.0,
            rot: q_hor * q_ver * roll,
        };
        // update next target position
        let delta = Decomposed {
            disp: disp.into(),
            ..Decomposed::one()
        };
        self.transform = delta.concat(&post.concat(&pre.concat(&self.transform)));
        // update the kinetic body
        self.target = next_pos.into();
        self.body.update(delta_disp.truncate(), next_pos, dt);

        if input.hit_count(three::Button::Key(three::Key::R)) > 0 {
            self.transform = self.initial_transform;
            self.body.reset_transform();
            self.accel = 0.0;
        }
        let pf: mint::Vector3<f32> = self.transform.disp.into();
        self.object.set_transform(pf, self.transform.rot, 1.0);
        self.position = [pf.x, pf.y, pf.z].into();
        self.target_orb.set_position([self.target.x, self.target.y + 10.0, self.target.z]);
    }
    pub fn gravitateTo(&mut self, center: &Point3<f32>) {
        let d = self.body.transform.disp;
        let to = Vector3::from(center - Point3::from([d.x, d.y, d.z])).normalize();
        let dw = 0.05;
        let to = [to.x * dw, to.y * dw, to.z * dw];
        let next_pos = [self.body.transform.disp.x + to[0], self.body.transform.disp.y + to[1], self.body.transform.disp.z + to[2]];
        self.body.group.set_position(next_pos);
        self.body.transform.disp = Vector3::from(next_pos);
        let delta = Decomposed {
            disp: to.into(),
            ..Decomposed::one()
        };
        let pf: mint::Vector3<f32> = self.transform.disp.into();
        self.transform = delta.concat(&self.transform);
        self.object.set_position(pf);
    }
}

fn get_delta_disp(input: &three::Input, time: f32) -> Vector4<f32>{
    let dt = time / 1000.0;
    let dt = 1.0;

    let mut dx = 0.0;
    let mut dy = 0.0;
    let mut dz = 0.0;
    let mut dw = 0.0;

    if input.hit(AXIS_A_D.pos) {
        dy += dt * 1.0;
    }
    if input.hit(AXIS_A_D.neg) {
        dy -= dt * 1.0;
    }
    if input.hit(AXIS_4_6.pos) {
        dz += dt * 1.0;
    }
    if input.hit(AXIS_4_6.neg) {
        dz -= dt * 1.0;
    }
    if input.hit(AXIS_8_5.neg) {
        dx -= dt * 1.0;
    }
    if input.hit(AXIS_8_5.pos) {
        dx += dt * 1.0;
    }
    if input.hit(AXIS_W_S.neg) {
        dw -= dt * 1.0;
    }
    if input.hit(AXIS_W_S.pos) {
        dw += dt * 1.0;
    }
    Vector4::new(dx, dy, dz, dw)
}
