use three::{
    self, Object,
    Shape, material,
    Geometry, Mesh, Group,
};
use mint; 
use super::planet_noise;
use cgmath::{Point3, Vector3, Quaternion, Rotation, prelude::MetricSpace,};
use genmesh::{EmitTriangles, Triangulate, Vertex as GenVertex};
use genmesh::generators::{self, IndexedPolygon, SharedVertex};
use super::IcoSphere;

pub struct Planet{
    pub position: Point3<f32>,
    radius: f32,
    lod: usize,
    lithosphere: Mesh, 
    hydrosphere: Mesh, 
    pub group: Group,
}

impl Planet {
    pub fn new(factory: &mut three::Factory, cam_position: Point3<f32>) -> Self {
        let group = factory.group();
        let radius = 6384.0;
        let material_pbr = |color| {
            material::Pbr {
                base_color_factor: color,
                roughness_factor: 1.0,
                metallic_factor: 0.0,
                ..Default::default()
            }
        };
        let material_wire = |color| {
            material::Wireframe {
                color: color,
                }
        };
        let position: Point3<f32> = [0.0, -radius, 0.0].into();
        let lod = (cam_position.distance(position) / radius) as usize;
        println!("LOD: {}", lod);
        let lithosphere = factory.mesh(gen_geometry(radius, 6, true), material_pbr(0x00FF00));
        let lithosphere_glow = factory.mesh_instance_with_material(&lithosphere, material_wire(0x00FF00));
        let hydrosphere = factory.mesh(gen_geometry(radius, 4, false), material_pbr(0x0000FF));
        let hydrosphere_glow = factory.mesh_instance_with_material(&hydrosphere, material_wire(0x0000FF));
        group.add(&lithosphere);
        group.add(&hydrosphere);
        group.add(&lithosphere_glow);
        group.add(&hydrosphere_glow);
        group.set_position([0.0,-radius, 0.0]);
        Planet {
            position,
            radius,
            lod,
            lithosphere,
            hydrosphere,
            group,
        }
    }
    pub fn update(&mut self, factory: &mut three::Factory, view_point: Point3<f32>) {
        // let d = view_point.distance(self.position);
        // println!("Distance from the center: {}", d);
        // let must_be_lod = (d / self.radius) as usize;
        // if self.lod != must_be_lod  && must_be_lod < 10 {
        //     self.group.remove(&self.lithosphere);
        //     self.lithosphere = factory.mesh_dynamic(geometry(self.radius, 10 - must_be_lod * 2, false), material::Wireframe{color: 0x00FF00});
        //     self.group.add(&self.lithosphere);
        //     l.msg(format!("LOD was {}. Changed to {}", self.lod, must_be_lod).as_str());
        //     self.lod = must_be_lod;
        // }
    }
}
pub fn gen_geometry(radius: f32, subdivision: usize, noisy: bool) -> Geometry {
let mut geo = generate(IcoSphere::subdivide(subdivision),//IcoSphere::subdivide(subdivision),
    |GenVertex { pos, .. }| pos.into(),
    |v| v.normal.into());
    if noisy {
        geo.base.vertices = planet_noise::displace(geo.base.vertices, radius);
    } else {
        geo.base.vertices = geo.base.vertices.iter().map(|pos|{
            [pos.x * radius, pos.y * radius, pos.z * radius].into()
        }).collect();
    }
    geo
}
fn generate<P, G, Fpos, Fnor>(
    gen: G,
    fpos: Fpos,
    fnor: Fnor,
) -> Geometry
where
    P: EmitTriangles<Vertex = usize>,
    G: IndexedPolygon<P> + SharedVertex<GenVertex>,
    Fpos: Fn(GenVertex) -> mint::Point3<f32>,
    Fnor: Fn(GenVertex) -> mint::Vector3<f32>,
{
    Geometry {
        base: Shape {
            vertices: gen.shared_vertex_iter().map(fpos).collect(),
            normals: gen.shared_vertex_iter().map(fnor).collect(),
            .. Shape::default()
        },
        // TODO: Add similar functions for tangents and texture coords
        faces: gen.indexed_polygon_iter()
            .triangulate()
            .map(|t| [t.x as u32, t.y as u32, t.z as u32])
            .collect(),
        .. Geometry::default()
    }
}