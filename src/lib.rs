#![warn(missing_docs)]
//! Here what follows is  bad english and bad computer codes. You may copy,
//! modify, redistribute modified or verbatim copies according to one of the
//! licenses, or both, mentioned below.
//! # Moksha
//! This is an experimental video game written in rust as a learning exercise. I
//! intended to use as much help from libraries that the rust community offers
//! to make a toy world with 3D graphics, pseudo physics, and sound effects.
//! ## How to ?
//! ### To run the game:
//! ```
//! git clone https://gitlab.com/nrayamajhee/moksha
//! cd moksha
//! cargo run
//! ```
//! ### To read docs:
//! ```
//! cargo doc --open
//! ```

extern crate three;
extern crate cgmath;
extern crate genmesh;
extern crate mint;
extern crate noise;
extern crate toml;
#[macro_use]
extern crate serde_derive;

pub mod log;
#[doc(inline)]
pub use log::Log;

pub mod world;
pub mod controls;
pub mod bodies;
pub mod planet;
