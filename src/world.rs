use three::{self, Object};
use std::path::PathBuf;
use std::fs;
use std::io::prelude::*;
use std::error::Error;
use toml;
use std::io::ErrorKind;

#[derive(Serialize, Deserialize, Clone)]
pub struct WinConfig {
    title: String,
    width: u32,
    height: u32,
    fullscreen: bool,
    multisampling: u16,
    vsync: bool,
    skybox: bool,
    skybox_path: String,
    skybox_ext: String,
}

pub struct World {
    config_path: PathBuf,
    default_config: WinConfig,
    pub config: WinConfig,
}

impl World {
    pub fn new(title: &str) -> Self {
        let config_path =  PathBuf::from(format!("{}/config/window.toml",
            env!("CARGO_MANIFEST_DIR")));
        let default_config = WinConfig {
            title: title.into(),
            width: 1920,
            height: 1080,
            fullscreen: true,
            multisampling: 2,
            vsync: true,
            skybox: true,
            skybox_path: String::from("data/Milkyway_Skybox"),
            skybox_ext: String::from("jpg"),
        };
        World {
            config_path,
            default_config: default_config.clone(),
            config: default_config,
        }
    }
    pub fn set_config_path(&mut self, path: &str) {
        let config_path =  PathBuf::from(format!("{}/{}",
            env!("CARGO_MANIFEST_DIR"), path));
        self.config_path = config_path;
    }
    pub fn window(&mut self) -> three::Window {
        let mut win_config_path = self.config_path.clone();
        if !win_config_path.exists() {
            println!("No config directory found. Creating it.");
            win_config_path.pop();
            match fs::create_dir_all(&win_config_path) {
                Ok(_) =>  println!("Created directory {} for default configurations.",
                                    win_config_path.display()),
                Err(why) => println!("Couldn't create {}. Not writing configuration to file: {}",
                                    win_config_path.display(), why.description()),
            }
        }
        match fs::File::open(&self.config_path) {
            Ok(mut file) => self.read_config(&mut file),
            Err(ref error) if error.kind() == ErrorKind::NotFound => {
                println!("{}\n{}\n{}",
                        "Couldn't find the configuration file for window",
                        "Creating one with default configuration",
                        error.description());
                self.write_config(&self.config);
            },
            Err(error) => {
                panic!("There was a problem opening the file: {:?}", error);
            },
        }
        let mut window = three::Window::builder(self.config.title.clone())
            .dimensions(self.config.width, self.config.height)
            .fullscreen(self.config.fullscreen)
            .multisampling(self.config.multisampling)
            .vsync(self.config.vsync)
            .build();
        if self.config.skybox {
            println!("Loading Cubemap...");
            self.load_cubemap(&mut window, self.config.skybox_path.as_str(), self.config.skybox_ext.as_str());
            println!("Skybox Loaded!");
        } else {
            window.scene.background = three::Background::Color(three::color::BLACK);
            println!("Not loading cubemap, black background loaded!");
        }
        window.set_cursor_state(three::CursorState::Hide);
        window
    }
    fn read_config(&mut self, file: &mut fs::File) {
        let mut s = String::new();
        match file.read_to_string(&mut s) {
            Ok(_) => {
                match toml::from_str(&s) {
                    Ok(config) => {
                        self.config = config;
                        println!("Configurations loaded from {}", self.config_path.display());
                    },
                    Err(error) => println!("{}\n{}\n{} {}\n{:?}",
                        "Couldn't parse the configurations.",
                        "Loading default configuration.",
                        "If the problem persists, consider deleting",
                        self.config_path.display(), error),
                }
            },
            Err(why) => println!("{}\n{}: {}",
                        "Couldn't read configurations.",
                        "Using default configurations",
                        why.description()),
        }
    }
    fn write_config(&self, config: &WinConfig) {
        match fs::File::create(&self.config_path) {
            Ok(mut file) => {
                match file.write_all(toml::to_string(config).unwrap().as_bytes()) {
                    Err(why) => println!("{}\n{}: {}",
                                    "Couldn't write configurations.",
                                    "Not writing configuration to file",
                                    why.description()),
                    Ok(_) => println!("Successfully wrote to {}", self.config_path.display()),
                }
            }
            Err(why) => println!("{}: {}.\n{}\nError: {}",
                                "Couldn't create file",
                                "Not writing configurations",
                                self.config_path.display(), why.description()),
        }
    }
    pub fn load_cubemap(&self, window: &mut three::Window, path: &str, ext: &str) {
        let full_path = format!("{}/{}", env!("CARGO_MANIFEST_DIR"),path);
        let skybox_path = three::CubeMapPath {
            front: format!("{}/{}.{}", full_path, "posz", ext), 
            back: format!("{}/{}.{}", full_path, "negz", ext), 
            up: format!("{}/{}.{}", full_path, "posy", ext), 
            down: format!("{}/{}.{}", full_path, "negy", ext), 
            left: format!("{}/{}.{}", full_path, "negx", ext), 
            right: format!("{}/{}.{}", full_path, "posx", ext), 
        };
        let skybox = window.factory.load_cubemap(&skybox_path);
        window.scene.background = three::Background::Skybox(skybox);
    }
    pub fn load_lights(&self, win: &mut three::Window) {
        let hemi_light = win.factory.hemisphere_light(0xaaaaaa, 0x000000, 0.9);
        win.scene.add(&hemi_light);
        let mut dir_light = win.factory.directional_light(0xffffff, 0.9);
        dir_light.look_at([150.0, 350.0, 350.0], [0.0, 0.0, 0.0], None);
        let shadow_map = win.factory.shadow_map(2048, 2048);
        dir_light.set_shadow(shadow_map, 400.0, 1.0 .. 1000.0);
        win.scene.add(&dir_light);
        let ambient_light = win.factory.ambient_light(0xdc8874, 1.0);
        win.scene.add(&ambient_light);
    }
}