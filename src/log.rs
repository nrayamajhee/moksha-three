use three;

pub struct Log {
    pub text: three::Text,
    pub fps_text: three::Text,
    fps : f32,
    text_str: String,
    on: bool,
}

impl Log {
    pub fn init(win: &mut three::Window) -> Self {
        let sans_light = win.factory.load_font(format!(
            "{}/data/fonts/Merriweather_Sans/MerriweatherSans-Light.ttf",
            env!("CARGO_MANIFEST_DIR")
        ));
        let text_str = "";
        let mut text = win.factory.ui_text(&sans_light, text_str);
        text.set_color(0xFFFFFF);
        text.set_opacity(0.0);
        text.set_font_size(24.0);
        text.set_pos([0.0,0.0]);
        let mut fps_text = text.clone();
        match win.glutin_window().get_inner_size() {
            Some((w,h)) => fps_text.set_pos([(w - 150) as f32,(h - 50) as f32]),
            None => println!("No dimensions!"),
        };
        win.scene.add(&text);
        Log {
            text,
            fps_text,
            fps: 0.0,
            text_str: text_str.to_string(),
            on: true,
        } 
    }
    pub fn msg(&mut self, msg: &str) {
        let new_text_str = format!("{}Log: {}\n", self.text_str, msg);
        self.text_str = new_text_str.clone();
        self.text.set_text(new_text_str);
        println!("{}", msg);
    }
    pub fn update(&mut self, input: &three::Input) {
        if input.hit_count(three::Button::Key(three::Key::Grave)) > 0 {
            self.on = !self.on;
            if self.on {
                self.text.set_opacity(1.0);
            } else {
                self.text.set_opacity(0.0);
            }
        }
    }
    pub fn fps(&mut self, dt: f32) {
        let fps = 1.0 / dt;
        if self.fps - fps > 5.0 || self.fps - fps < -5.0 {
            self.fps_text.set_text(format!("{}", (1.0 / dt) as usize));
            self.fps = fps;
        }
    }
}
