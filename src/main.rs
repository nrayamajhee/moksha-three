extern crate three;
extern crate moksha;

use three::{Object};
use moksha::{world, log, controls, planet};

fn main(){
    let mut world = world::World::new("Moksha");
        // .set_config_path("config/window.toml");

    let mut win = world.window();

    let mut l = log::Log::init(&mut win);
    // world.load_cubemap(&mut win, "data/Milkyway_Skybox", "jpg");
    world.load_lights(&mut win);

    let cam = win.factory.perspective_camera(60.0, 1.0 .. 100000.0);
    let mut controls = controls::DynamicController::new(&mut win.factory, &cam);
    win.scene.add(&controls.body.group);
    win.scene.add(&controls.target_orb);

    l.msg("Creating Planet...");
    let planet = planet::Planet::new(&mut win.factory, controls.position);
    l.msg("Planet creation completed!");
    win.scene.add(&planet.group);

    while win.update() && !win.input.hit(three::KEY_ESCAPE) {
        let dt = win.input.delta_time();
        l.fps(dt);
        l.update(&win.input);
        controls.update(&win.input, dt);
        // controls.gravitateTo(&planet.position);
        win.render(&cam);
    }
}