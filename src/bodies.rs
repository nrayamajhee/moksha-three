use three::{
    self, Group, Object,
};
use mint;
use cgmath::{
    Decomposed, Vector3, 
    Quaternion, Rotation, 
    Rotation3, Rad,
};

pub struct KinematicBody {
    pub group: Group,
    mixer: three::animation::Mixer,
    pub transform: Decomposed<Vector3<f32>, Quaternion<f32>>,
    initial_transform: Decomposed<Vector3<f32>, Quaternion<f32>>,
}

impl KinematicBody {
    pub fn new(factory: &mut three::Factory, path: &str) -> Self {
        let mut mixer = three::animation::Mixer::new();
        let body = {
            let group = factory.group();
            let gltf = factory.load_gltf(&path);
            // for instance in gltf.instances {
            //     let instance_glow = factory.mesh_instance_with_material(&instance, three::material::Wireframe{color: 0xFFFFFF});
            //     group.add(&instance);
            //     group.add(&instance_glow);
            // }
            group.add(&gltf);
            for clip in gltf.clips {
                mixer.action(clip);
            }
            group
        };
        let transform = Decomposed {
            disp: Vector3::from([0.0,0.0,0.0]),
            rot: Quaternion::look_at(Vector3::unit_z(), Vector3::unit_y()),
            scale: 1.0,
        };
        KinematicBody {
            group: body,
            mixer,
            transform: transform.clone(),
            initial_transform: transform,
        }
    }
    pub fn update(&mut self, euler_angles: Vector3<f32>, next_pos: [f32; 3], dt: f32) {
        // set orientation
        let pitch = Quaternion::from_angle_x(Rad(0.5 * dt * euler_angles.x));
        let yaw = Quaternion::from_angle_y(Rad(0.5 * dt * -euler_angles.y));
        let roll = Quaternion::from_angle_z(Rad(0.5 * dt * euler_angles.z));
        let rot = self.transform.rot * pitch * yaw * roll;
        self.group.set_orientation(rot);
        self.transform.rot = rot;

        // set position
        self.group.set_position(next_pos);
        self.transform.disp = Vector3::from(next_pos);

        // udpate animation mixer
        self.mixer.update(dt);
    }
    pub fn reset_transform(&mut self) {
        self.transform = self.initial_transform;
        let pf: mint::Vector3<f32> = self.transform.disp.into();
        self.group.set_transform(pf,self.initial_transform.rot, self.initial_transform.scale);
    }
}