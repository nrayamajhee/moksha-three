Here what follows is  bad english and bad computer codes. You may copy, modify, redistribute modified or verbatim copies according to one or both of the licenses mentioned below.

# Moksha

This is an experimental video game written in rust as a learning exercise. I intended to use as much help from libraries that the rust community offers to make a toy world with 3D graphics, pseudo physics, and sound effects.

![screenshot](data/img/screenshot.png)
# Task List

- World
    - [x] Create window, load milkyway skybox, and add plane model.
    - [x] Use configuration file to creating window.
    - [x] Use config file for loading skybox.
    - [x] Create a debug mode that renders logs to the game window
    - [x] Create a fps and debug text screen
    - [ ] Crate a 3D UI to display window config
- Plane
    - [x] Custom third person perspective camera 
    - [x] Basic movement
    - [x] Accelerate, Deaccelerate, Roll, Pitch, and yaw
    - [ ] Gravity
- Planet
    - [x] Displace the icosphere vertices with noise function
    - [ ] Multithread noise function and displacment
    - [ ] Create noise function once, and use it many times to update the planet
        geometry
    - [ ] Implement level of detail for icosphere vertices.

## How to?

To run the game:
```bash
git clone https://gitlab.com/nrayamajhee/moksha.git
cd moksha
cargo run
```

To read offline docs:

```bash
cargo doc --open
```
## License

Licensed under either of the following terms at your choice:

  - Apache License, Version 2.0, (LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0)
  - MIT license (LICENSE-MIT or http://opensource.org/licenses/MIT)

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.

Data contained in the repository (images, gltf, blend, etc.) are licensed under Creative Commons Attribution 4.0 International License (CC BY 4.0).
Refer to https://creativecommons.org/licenses/by/4.0/ for the details.

Please refer to the links at /data/credits for individual attributions for the blend file used to generate the skybox, and the deep star map from NASA.
